# Objgraphtest

Objectgraphtest provides testing capabilities to test an object graph.

Objgraphtest is a custom-made testing library, focused on verifying that data is parsed 
correctly. instead of writing dozens of arrange-act-assert tests, you simply write a table
that looks like the output you want:

```lua
graph = Save {
    monsterStorage = MonsterStorage {
        [selectLength] = 14,
        pcStorage = {
            [selectLength] = 12,
            [13] = match nil,
        },
        team = Box {
            capacity = 6,
            [1] = Monster {
                species = 16,
                experience = 1059860,
                originalTrainerName = "RED",
                [selectSerializedTo(usTeamUUID)] = matchFileContents("data/serialized/oldbird.bin")
            }
            [2] = Monster {},
            [5] = match nil,
        }
    }
}
```

## Concepts

Each object graph is made up of matchers, selectors and descriptions. A matcher is
simply something that checks if a value is correct (e.g. `matchType('table')` or `match nil`).
Matchers are also implicitly created from non-table values (e.g. `5` becomes `match 5`).
Every object graph must be a matcher.

## Description

Descriptions are special matchers, that check if a table (or userdata) has certain properties. To do so,
it uses selectors (to select values from the object that is being inspected) and matchers (to check if those
values are correct). Descriptions are usually just a table with key, value pairs: `{ foo = "bar" }` would match
every object where the key `"foo"` has the value `"bar"`.

## Selector

Selectors are used in descriptions to specify which part of an object you want to check.
The typical example is simply a key: `asSelector "foo"` would select `object.foo`. Other, more
imaginative selectors might select the object's length (`LengthSelector`) or a method's return value.
