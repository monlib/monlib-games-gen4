local *

-- moonscript's `*`, but without the parsing errors
enumerate = (t) ->
    i = 0
    ->
        i += 1
        if i <= #t then t[i]


class ImmStack
    new: (...) =>
        @n = select("#", ...)
        table.move({...}, 1, @n, 1, @)
    __len: => @n
    push: (...) =>
        if 0 < @n
            ImmStack(table.unpack(@, 1, @n), ...)
        else
            ImmStack(...)
    pop: =>
        assert self.n >= 1
        (ImmStack table.unpack(@, 1, @n - 1)), @\peek!
    peek: => @[@n]
    at: (i) => @[i]
    empty: => #@ == 0
    __tostring: =>
        s = "stack: { "
        for i = @n, 1, -1 do
            s = s .. "#{@[i]}, "
        s .. "}"


{:ImmStack, :enumerate}