gen4 = require 'monlib.games.gen4'
import match from require 'objgraphtest'
import ImmStack from require 'util'

-- List of all objgraphtest source files.
objgraphs = {}

registerGraph = (filename) ->
    {:graph, :saveFile, :friendlyName} = assert pcall assert loadfile(filename, "t", env)
    describe("object graph of #{friendlyName}", ->
        tests = (match graph)\toTest ImmStack!
        tests gen4.Save.new filename)

for _, graph in *objgraphs do registerGraph graph
