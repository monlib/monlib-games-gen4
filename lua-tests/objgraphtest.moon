local *

import enumerate, ImmStack from require "util"

isMatcher = {}
isSelector = {}

--- Matcher
-- This is the base class for all matchers, it's main purpose is
-- documentation and the ability to identify matchers via the
-- `isMatcher` property.
class Matcher
    [isMatcher]: true
    --- toTest :: ImmStack<Description> -> object -> nil
    -- toTest processes the matcher into a state where it can check
    -- if any given object matches. Since matchers may need to acceess
    -- other parts of the object graph, they are provided with a stack of
    -- all descriptors between them and the root.
    toTest: (hist) =>
        error("matcher without toTest at #{hist}")


--- Selector
-- This is the base class for all selectors, it's main purpose is
-- documentation and the ability to indentify selectors via the
-- `isSelector` property.
class Selector
    [isSelector]: true
    --- select :: (object, ImmStack<Description>) -> Iterator<{object, string}>`
    -- select returns an iterator over tuples of objects to be inspected and
    -- an associated descriptive string.
    select: (obj, hist) =>
        error("selector without select at #{hist}")


--- Description
-- This is the base class for all descriptions. Descriptions are generated
-- as matchers from all tables which are not matchers themselves.
-- Matchers 
class Description extends Matcher
    --- new :: table -> Description
    -- Converts the table into a description using `asSelector`
    -- and `match`.
    new: (t) =>
        super!
        @t = {asSelector k, match v for k, v in pairs t}

    --- toTest
    -- Ensures that the object is a table and then calls each matcher
    -- for each selected value. Each called selector or matcher will find
    -- this description object at the top of the stack.
    toTest: (hist) =>
        hist = hist\push(@)
        makeTest = (sel, mat) ->
            test = mat\toTest hist
            (obj) ->
                for {value, desc} in sel\select(obj, hist)
                    describe(desc, -> test value)
        tests = [makeTest(sel, mat) for sel, mat in pairs @t]
        (obj) ->
            describe("is a table", ->
                assert.true_(type obj == 'table' or type obj == 'userdata')
                describe("where", -> for test in *tests do test obj)
            )

--- LiteralMatcher
-- matches the given object using `assert.are.same`. Used by default for
-- all non-table values.
class LiteralMatcher extends Matcher
    new: (@obj) => super!
    toTest: => (obj) -> it("has value `#{@obj}`", -> assert.are.same(@obj, obj))


--- LiteralSelector
-- Selects the value at the given key for the inspected object.
class LiteralSelector extends Selector
    new: (@key) => super!
    select: (obj) => enumerate {{obj[@key], "index `#{@key}`"}}


--- asSelector :: object -> Selector
-- Converts any object into a selector. If the object already is a selector,
-- it is returned unchanged, otherwise it is used as the key in a LiteralSelector.
asSelector = (obj) ->
    if (type obj == 'table' or type obj == 'userdata') and obj[isSelector]
        obj
    else
        LiteralSelector obj


--- match :: object -> Matcher
-- Converts any object into a matcher. If the object already is a matcher,
-- it is returned unchanged, otherwise tables become Descriptions and all
-- other values become LiteralMatchers.
match = (obj) ->
    if (type obj == 'table' or type obj == 'userdata') and obj[isMatcher]
        obj
    elseif type obj == 'table'
        Description obj
    else
        LiteralMatcher obj


--- EqualityMatcher
-- matches with the given object using `assert.are.equal`, that is using `==`.
class EqualityMatcher extends Matcher
    new: (@obj) => super!
    toTest: => (obj) -> it("equals `#{@obj}`", -> assert.are.equal(@obj, obj))


--- IdentityMatcher
-- matches if the given object is identical to the inspected one, using `rawequal`.
class IdentityMatcher extends Matcher
    new: (@obj) => super!
    toTest: => (obj) -> it("is identical to `#{@obj}`", -> assert.true_ rawequal(obj, @obj))


--- LengthSelector
-- Selects the length of the object using the `#` operator.
class LengthSelector extends Selector
    new: => super!
    select: (obj) => enumerate {{#obj, "its length"}}


--- DeclaredSequenceSelector
-- Selects the values of the object's sequence (i.e. obj[1], obj[2], ..., obj[#obj])
-- where #obj is determined by the value associated with the description's lengthKey.
-- this is just a messy POC for history inspection.
class DeclaredSequenceSelector extends Selector
    new: (@lengthKey) => super!
    select: (obj, hist) =>
        len = hist\peek!.t[@lengthKey].obj -- super messy, needs cleanup.
        enumerate [{obj[i], "index `#{i}`"} for i=1, len]

{:isMatcher, :Matcher, :isSelector, :Selector, :Description, :LiteralMatcher,
LiteralSelector, :asSelector, :match, :EqualityMatcher, :IdentityMatcher,
LengthSelector, :DeclaredSequenceSelector}
