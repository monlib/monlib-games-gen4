# Lua bindings for monlib.games.gen4

This C library binds monlib.games.gen4 to rust. This code was not written in Rust
because it's too easy to write code that is broken by Lua's longjmps.

All lua_CFunctions must be documented in the same manner as in the lua reference manual.
