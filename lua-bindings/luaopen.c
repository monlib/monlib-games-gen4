#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

#include <assert.h>
#include <stdint.h>

extern int32_t monlib_games_gen4_foo(int32_t);

static int gen4_impl_save_new(lua_State* L);
static int gen4_raise_out_of_stack(lua_State* L);
static int gen4_setup_save(lua_State* L);

/// Entry point for the module.
/// Returns the module table and performs other setup.
/// [-0, +1, e]
int luaopen_monlib_games_gen4(lua_State* L)
{
    const int max_pushed_values = 5;
    int initial_stack_top = lua_gettop(L);
    if (lua_checkstack(L, max_pushed_values)) {
        lua_createtable(L, 0, 1);
        gen4_setup_save(L);
        lua_setfield(L, -2, "Save");
        assert(lua_gettop(L) - initial_stack_top == 1);
        return 1;
    } else {
        return gen4_raise_out_of_stack(L);
    }
}

/// Creates the Save class and returns its table.
/// [-0, +1, e]
int gen4_setup_save(lua_State* L)
{
    const int max_pushed_values = 5;
    int initial_stack_top = lua_gettop(L);
    if (lua_checkstack(L, max_pushed_values)) {
        lua_createtable(L, 0, 1);
        lua_pushcfunction(L, &gen4_impl_save_new);
        lua_setfield(L, -2, "new");
        assert(lua_gettop(L) - initial_stack_top == 1);
        return 1;
    } else {
        return gen4_raise_out_of_stack(L);
    }
}

/// Raises an out-of-stack error.
/// [--, --, v]
int gen4_raise_out_of_stack(lua_State* L)
{
    // Ensure enough stack space to error
    if (!lua_checkstack(L, 1)) {
        // This operation should not fail because 2 < LUA_MINSTACK
        lua_settop(L, 2);
    }
    assert(lua_checkstack(L, 1));
    lua_pushliteral(L, "Error: ran out of lua stack space!");
    return lua_error(L);
}

/// Creates a new save object.
/// Currently just a dummy.
/// [-0, +1, e]
int gen4_impl_save_new(lua_State* L)
{
    const int max_pushed_values = 1;
    int initial_stack_top = lua_gettop(L);
    if (lua_checkstack(L, max_pushed_values)) {
        lua_createtable(L, 0, 1);
        lua_pushinteger(L, monlib_games_gen4_foo(29));
        lua_setfield(L, -2, "foo");
        assert(lua_gettop(L) - initial_stack_top == 1);
        return 1;
    } else {
        gen4_raise_out_of_stack(L);
    }
}
