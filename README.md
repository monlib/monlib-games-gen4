# monlib-games-gen4

This repository contains an implementation of Monlib.Core for the mainline generation 4 games
(D/P/Pt/HG/SS). The project is implemented in a mix of Rust and C (for gluing to Lua) and currently
in an experimental state. It is unclear, wether a Rust implementation will run on all desired targets.

This is also my first rust project so expect some horrible code.

## License

All files in this repository are licensed under the GPL version 3 or greater. A copy
is included in the `LICENSE` file.
