from conans import ConanFile, CMake, tools
import shutil
import pathlib

class Monlibgamesgen4Conan(ConanFile):
    name = "monlib.games.gen4"
    version = "0.1.0"
    license = "GPLv3+"
    url = "https://gitlab.com/monlib/monlib-games-gen4.git"
    description = "A Lua module to edit gen4 save data"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "withTests": [True, False],
    }
    default_options = (
        "shared=False",
        "fPIC=False",
        "withTests=False"
    )
    generators = "cmake_find_package"
    exports_sources = 'src/*', 'lua-bindings/*', 'test/*', 'Cargo.toml', 'lua-tests/*'
    requires = ('lua/5.3.4@monlib/stable', )

    def build(self):
        print(self.settings.build_type)
        self.run('cargo test')
        self.run('cargo build {} --target-dir cargo-build'.format('' if self.settings.build_type == 'Debug' else '--release'))
        shutil.move('cargo-build/{}/libmonlib_games_gen4.a'.format('debug' if self.settings.build_type == 'Debug' else 'release'),
                'libmonlib_games_gen4.a')
        cmake = CMake(self)
        cmake.configure(source_folder='lua-bindings')
        cmake.build()
        if self.options.shared == True:
            pathlib.Path('monlib/games').mkdir(parents=True, exist_ok=True)
            pathlib.Path('libmon-games-gen4-lua.so').rename('monlib/games/gen4.so')
            if self.options.withTests: self.run('busted -C lua-tests --cpath=../?.so main.moon')

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*mon-games-gen4-lua.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=True)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["mon-games-gen4-lua"]

    def configure(self):
        del self.settings.compiler.libcxx
